﻿using GlobalShippingSolutions.Infrastructure;
using GlobalShippingSolutions.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GlobalShippingSolutions.Controllers
{
    public class GetQuotesController : Controller
    {
        //AirCargo
        [HttpGet]
        public ActionResult AirCargo()
        {
            AirCargoQuote model = new AirCargoQuote();
            model.PopulateDropDownLists();
            return View(model);
        }

        [HttpPost]
        public ActionResult AirCargo(AirCargoQuote model)
        {
            GenericResultModel result = new GenericResultModel();

            if (ModelState.IsValid)
            {
                EmailModel mainEmail = new EmailModel();

                AppendGenericAttributes((GenericQuoteModel)model, "Air Cargo", ref mainEmail);
                AppendServicesRequired((GenericQuoteModel)model, ref mainEmail);

                mainEmail.Body += "<br />Dimensions : " + model.Dimensions +
                "<br />Class : " + model.Classx +
                "<br />Comments :" + model.Comments;

                mainEmail.Body += "</body></html>";

                AsyncSendingEmail ASendAdminEmail = new AsyncSendingEmail(mainEmail, true);
                ASendAdminEmail.SendAsynchronously();

                SendAcknowlegment((GenericQuoteModel)model);

                result.Success = true;
            }
            else
            {
                result.Success = false;
                result.ErrorCode = 10;
                result.ModelErrorDetails = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToList();
            }

            return Json(result);
        }

        //OceanFreight
        [HttpGet]
        public ActionResult OceanFreight()
        {
            OceanFreightQuote model = new OceanFreightQuote();
            model.PopulateDropDownLists();
            return View(model);
        }

        [HttpPost]
        public ActionResult OceanFreight(OceanFreightQuote model)
        {
            GenericResultModel result = new GenericResultModel();

            if (ModelState.IsValid)
            {
                EmailModel mainEmail = new EmailModel();

                AppendGenericAttributes((GenericQuoteModel)model, "Ocean Freight", ref mainEmail);
                AppendServicesRequired((GenericQuoteModel)model, ref mainEmail);

                mainEmail.Body += "<br />No. of Pallets, Rolls or Boxes : " + model.PalletsRollBoxes +
                "<br />Equipment Required : " + model.Equipment +
                "<br />Container Length : " + model.Length +
                "<br />Description : " + model.ShippingDescription;

                mainEmail.Body += "</body></html>";

                AsyncSendingEmail ASendAdminEmail = new AsyncSendingEmail(mainEmail, true);
                ASendAdminEmail.SendAsynchronously();

                SendAcknowlegment((GenericQuoteModel)model);

                result.Success = true;
            }
            else
            {
                result.Success = false;
                result.ErrorCode = 10;
                result.ModelErrorDetails = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToList();
            }

            return Json(result);
        }

        //Trucking
        [HttpGet]
        public ActionResult Trucking()
        {
            TruckingQuote model = new TruckingQuote();
            model.PopulateDropDownLists();
            return View(model);
        }

        [HttpPost]
        public ActionResult Trucking(TruckingQuote model)
        {
            GenericResultModel result = new GenericResultModel();

            if (ModelState.IsValid)
            {
                EmailModel mainEmail = new EmailModel();

                AppendGenericAttributes((GenericQuoteModel)model, "Trucking", ref mainEmail);
                AppendServicesRequired((GenericQuoteModel)model, ref mainEmail);

                mainEmail.Body += "<br />No. of Pallets, Rolls or Boxes : " + model.PalletsRollBoxes +
                "<br />Equipment Required : " + model.Equipment +
                "<br />Trailer Length : " + model.Length +
                "<br />Comments : " + model.Comments +
                "<br />Description : " + model.ShippingDescription;

                mainEmail.Body += "</body></html>";

                AsyncSendingEmail ASendAdminEmail = new AsyncSendingEmail(mainEmail, true);
                ASendAdminEmail.SendAsynchronously();

                SendAcknowlegment((GenericQuoteModel)model);

                result.Success = true;
            }
            else
            {
                result.Success = false;
                result.ErrorCode = 10;
                result.ModelErrorDetails = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToList();
            }

            return Json(result);
        }

        //Warehousing
        [HttpGet]
        public ActionResult Warehousing()
        {           
            return View();
        }
        
        //RailShipment
        [HttpGet]
        public ActionResult RailShipment()
        {
            RailShipmentQuote model = new RailShipmentQuote();
            model.PopulateDropDownLists();
            return View(model);
        }

        [HttpPost]
        public ActionResult RailShipment(RailShipmentQuote model)
        {
            GenericResultModel result = new GenericResultModel();

            if (ModelState.IsValid)
            {
                EmailModel mainEmail = new EmailModel();

                AppendGenericAttributes((GenericQuoteModel)model, "Rail Shipment", ref mainEmail);
                AppendServicesRequired((GenericQuoteModel)model, ref mainEmail);

                mainEmail.Body += "<br />No. of Pallets, Rolls or Boxes : " + model.PalletsRollBoxes +               
                "<br />Description : " + model.ShippingDescription;

                mainEmail.Body += "</body></html>";

                AsyncSendingEmail ASendAdminEmail = new AsyncSendingEmail(mainEmail, true);
                ASendAdminEmail.SendAsynchronously();

                SendAcknowlegment((GenericQuoteModel)model);

                result.Success = true;
            }
            else
            {
                result.Success = false;
                result.ErrorCode = 10;
                result.ModelErrorDetails = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToList();
            }

            return Json(result);
        }

        //LTLShipments
        [HttpGet]
        public ActionResult LTLShipments()
        {
            LTLShipmentsQuote model = new LTLShipmentsQuote();
            model.PopulateDropDownLists();
            return View(model);
        }

        [HttpPost]
        public ActionResult LTLShipments(LTLShipmentsQuote model)
        {
            GenericResultModel result = new GenericResultModel();

            if (ModelState.IsValid)
            {
                EmailModel mainEmail = new EmailModel();

                AppendGenericAttributes((GenericQuoteModel)model, "LTLShipments", ref mainEmail);
                AppendServicesRequired((GenericQuoteModel)model, ref mainEmail);

                mainEmail.Body += "<br />Dimensions : " + model.Dimensions +
                "<br />No. of Pieces : " + model.PiecesNo +
                "<br />Description : " + model.ShippingDescription;                

                mainEmail.Body += "</body></html>";

                AsyncSendingEmail ASendAdminEmail = new AsyncSendingEmail(mainEmail, true);
                ASendAdminEmail.SendAsynchronously();

                SendAcknowlegment((GenericQuoteModel)model);

                result.Success = true;
            }
            else
            {
                result.Success = false;
                result.ErrorCode = 10;
                result.ModelErrorDetails = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToList();
            }

            return Json(result);
        }


        private void SendAcknowlegment(GenericQuoteModel model)
        {
            EmailModel userEmail = new EmailModel();
            userEmail.To = model.Email;
            userEmail.Subject = "Global Shipping Solution - Acknowledgment";
            userEmail.Body = "<html><body>Dear " + model.ContactName +
                "<br /><br />Thank you so much for contacting Global Shipping Solutions, relevant person will be in touch with you soon" +
                "<br /><br />Regards" +
                //"<br /><br /><a href=\"http://www.fastecsol.com\"><img src=\"http://www.fastecsol.com/Content/images/logo.png\" alt=\"Logo\"></a>" +
                "</body></html>";

            AsyncSendingEmail ASendUserEmail = new AsyncSendingEmail(userEmail, true);
            ASendUserEmail.SendAsynchronously();
        }

        private void AppendGenericAttributes(GenericQuoteModel model, string type, ref EmailModel mainEmail)
        {
            mainEmail.To = System.Configuration.ConfigurationManager.AppSettings["AdminEmail"];
            mainEmail.Subject = "Global Shipping Solution - " + type;
            mainEmail.Body = "<html><body>Company Name : " + model.CompanyName +
                "<br />Email : " + model.Email +
                "<br />Contact Name : " + model.ContactName +
                "<br />Phone : " + model.Phone +
                "<br />Shipping From : " + model.ShippingFrom +
                "<br />Shipping To : " + model.ShippingTo +
                "<br />Postal/Zip From : " + model.PostalZipFrom +
                "<br />Postal/Zip To : " + model.PostalZipTo +
                "<br />City/State/Country From : " + model.CityStateCountryFrom +
                "<br />City/State/Country To : " + model.CityStateCountryTo +
                "<br />Load Type : " + model.LoadType +
                "<br />Weight : " + model.Weight;                
        }

        private void AppendServicesRequired(GenericQuoteModel model, ref EmailModel mainEmail)
        {
            if (model.TeamService || model.Liftgate || model.ResidentialDelivery || model.DriverLoad || model.DriverUnload || model.Other)
            {
                mainEmail.Body += "<br />Services Required : ";
                mainEmail.Body += model.TeamService ? "TeamService, " : string.Empty;
                mainEmail.Body += model.Liftgate ? "Liftgate, " : string.Empty ;
                mainEmail.Body += model.ResidentialDelivery ? "ResidentialDelivery, " : string.Empty;
                mainEmail.Body += model.DriverLoad ? "DriverLoad, " : string.Empty;
                mainEmail.Body += model.DriverUnload ? "DriverUnload, " : string.Empty;
                mainEmail.Body += model.Other ? "Other, " : string.Empty;
                mainEmail.Body = mainEmail.Body.Substring(0, mainEmail.Body.Length - 1);
                
            }            
        }
    }
}