﻿using GlobalShippingSolutions.Infrastructure;
using GlobalShippingSolutions.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GlobalShippingSolutions.Controllers
{
    public class ContactUsController : Controller
    {
        //ContactUs
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(FeedbackModel model)
        {
            GenericResultModel result = new GenericResultModel();
           
            if (ModelState.IsValid)
            {
                EmailModel contactUsEmail = new EmailModel();
                contactUsEmail.To = System.Configuration.ConfigurationManager.AppSettings["AdminEmail"];
                contactUsEmail.Subject = "Global Shipping Solution - Contact Us";
                contactUsEmail.Body = "<html><body>Name : " + model.Name +
                    "<br />Email : " + model.Email +
                    "<br />Phone :" + model.Phone +
                    "<br />Comments :" + model.Comments +                    
                    "</body></html>";

                AsyncSendingEmail ASendAdminEmail = new AsyncSendingEmail(contactUsEmail, true);
                ASendAdminEmail.SendAsynchronously();

                EmailModel userEmail = new EmailModel();
                userEmail.To = model.Email;
                userEmail.Subject = "Global Shipping Solution - Acknowledgment";
                userEmail.Body = "<html><body>Dear " + model.Name +
                    "<br /><br />Thank you so much for contacting Global Shipping Solutions, relevant person will be in touch with you soon" +
                    "<br /><br />Regards" +
                    //"<br /><br /><a href=\"http://www.fastecsol.com\"><img src=\"http://www.fastecsol.com/Content/images/logo.png\" alt=\"Logo\"></a>" +
                    "</body></html>";

                AsyncSendingEmail ASendUserEmail = new AsyncSendingEmail(userEmail, true);
                ASendUserEmail.SendAsynchronously();
                    
                result.Success = true;
            }
            else
            {
                result.Success = false;
                result.ErrorCode = 10;
                result.ModelErrorDetails = ModelState.Where(x => x.Value.Errors.Count > 0).Select(x => new { x.Key, x.Value.Errors }).ToList();
            }            

            return Json(result);
        }

    }
}