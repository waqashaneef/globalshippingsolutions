﻿$(window).load(function () { // makes sure the whole site is loaded   
    if (typeof showNotification == 'function') {
        var enableNotify = Cookies.get('EnableNotify');
        if (enableNotify) {
            var enableNotify = Cookies.get('EnableNotify');
            setTimeout(showNotification(enableNotify, "notification"), 1000);
        }
    }
})

function showNotification(message, type) {
    var n = noty({        
        text: message,
        type: type,
        timeout: false,
        maxVisible: 5,
        buttons: false, // an array of buttons
        dismissQueue: true,
        theme: 'notyTheme',
        closeWith: ['button', 'click'],
        animation: {
            open: { height: 'toggle' }, // jQuery animate function property object
            close: { height: 'toggle' }, // jQuery animate function property object
            easing: 'swing', // easing
            speed: 1000 // opening & closing animation speed
        },
        callback: {
            onShow: function () {
                Cookies.remove('EnableNotify');
                setTimeout(function () {
                    $.noty.closeAll();
                }, 5000);
            }
        }
    });
}