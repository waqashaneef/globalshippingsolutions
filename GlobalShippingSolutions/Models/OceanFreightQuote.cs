﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GlobalShippingSolutions.Models
{
    public class OceanFreightQuote : GenericQuoteModel
    {
        [MaxLength(100)]
        [Display(Name = "No. of Pallets, Rolls or Boxes: ")]
        [Required(ErrorMessage = "* Required")]
        [StringLength(100, ErrorMessage = "* Length")]
        public string PalletsRollBoxes { set; get; }

        public IEnumerable<Equipment> EquipmentList { set; get; }

        [Required(ErrorMessage = "* Required")]
        [MaxLength(50)]
        [Display(Name = "Equipment Required: ")]
        public string Equipment { set; get; }

        public IEnumerable<Length> LengthList { set; get; }

        [Required(ErrorMessage = "* Required")]
        [MaxLength(50)]
        [Display(Name = "Container Length: ")]
        public string Length { set; get; }


        [Display(Name = "Please Describe What You Are Shipping: ")]
        public string ShippingDescription { set; get; }


    }

    public static class OceanFreightQuoteExtentions
    {
        public static OceanFreightQuote PopulateDropDownLists(this OceanFreightQuote model)
        {

            model.LoadTypeList = new List<LoadType>
            {
                new LoadType() {Value = "Palletized", Text = "Palletized"},
                new LoadType() {Value = "Rolls", Text = "Rolls"},
                new LoadType() {Value = "Balles", Text = "Balles"},
                new LoadType() {Value = "Floor", Text = "Floor"}, 
                new LoadType() {Value = "Other", Text = "Other"} 
            };

            model.EquipmentList = new List<Equipment>
            {
                new Equipment() {Value = "Dry Box", Text = "Dry Box"},
                new Equipment() {Value = "Refrigeraterd Box", Text = "Refrigeraterd Box"}               
            };

            model.LengthList = new List<Length>
            {
                new Length() {Value = "Box 20'", Text = "Box 20'"},
                new Length() {Value = "Box 40' STD", Text = "Box 40' STD"}, 
                new Length() {Value = "Box 40' HC", Text = "Box 40' HC"},
                new Length() {Value = "Box 45'", Text = "Box 45'"} 
            };

            return model;
        }
    }
}