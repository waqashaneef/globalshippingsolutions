﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GlobalShippingSolutions.Models
{
    public class GenericQuoteModel
    {        
        [MaxLength(100)]
        [Display(Name = "Company Name: ")]
        [Required(ErrorMessage = "* Required")]
        [StringLength(100, ErrorMessage = "* Length")]  
        public string CompanyName { set; get; }

        [MaxLength(250)]
        [Display(Name = "Email: ")]
        [Required(ErrorMessage = "* Required")]
        [StringLength(250, ErrorMessage = "* Length")]        
        [RegularExpression(@"^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$", ErrorMessage = "* Invalid Value")]
        public string Email { set; get; }

        [MaxLength(100)]
        [Display(Name = "Contact Name: ")]
        [Required(ErrorMessage = "* Required")]
        [StringLength(100, ErrorMessage = "* Length")]
        public string ContactName { set; get; }

        [MaxLength(20)]
        [Display(Name = "Phone: ")]
        [Required(ErrorMessage = "* Required")]
        [StringLength(20, ErrorMessage = "* Length")]       
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "* Invalid Value")]
        public string Phone { set; get; }


        [MaxLength(250)]
        [Display(Name = "Shipping From: ")]
        [Required(ErrorMessage = "* Required")]
        [StringLength(250, ErrorMessage = "* Length")]
        public string ShippingFrom { set; get; }

        [MaxLength(250)]
        [Display(Name = "Shipping To: ")]
        [Required(ErrorMessage = "* Required")]
        [StringLength(250, ErrorMessage = "* Length")]
        public string ShippingTo { set; get; }

        [MaxLength(20)]
        [Display(Name = "Postal/Zip Code: ")]
        [Required(ErrorMessage = "* Required")]
        [StringLength(20, ErrorMessage = "* Length")]        
        public string PostalZipFrom { set; get; }

        [MaxLength(20)]
        [Display(Name = "Postal/Zip Code: ")]
        [Required(ErrorMessage = "* Required")]
        [StringLength(20, ErrorMessage = "* Length")]        
        public string PostalZipTo { set; get; }

        [MaxLength(250)]
        [Display(Name = "City/State/Country:")]
        [Required(ErrorMessage = "* Required")]
        [StringLength(250, ErrorMessage = "* Length")]
        public string CityStateCountryFrom { set; get; }

        [MaxLength(250)]
        [Display(Name = "City/State/Country: ")]
        [Required(ErrorMessage = "* Required")]
        [StringLength(250, ErrorMessage = "* Length")]
        public string CityStateCountryTo { set; get; }

        public IEnumerable<LoadType> LoadTypeList { set; get; }

        [Required(ErrorMessage = "* Required")]
        [MaxLength(50)]
        [Display(Name = "Load Type: ")]
        public string LoadType { set; get; }


        [MaxLength(10)]
        [Display(Name = "Weight : ")]
        [Required(ErrorMessage = "* Required")]
        [StringLength(10, ErrorMessage = "* Length")]
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "* Invalid Value")]
        public string Weight { set; get; }

        [Display(Name = "Team Service")]
        public bool TeamService { get; set; }
        [Display(Name = "Liftgate")]
        public bool Liftgate { get; set; }
        [Display(Name = "Residential Delivery")]
        public bool ResidentialDelivery { get; set; }
        [Display(Name = "Driver Load")]
        public bool DriverLoad { get; set; }
        [Display(Name = "Driver Unload")]
        public bool DriverUnload { get; set; }
        [Display(Name = "Other (Use Comments)")]
        public bool Other { get; set; }
    }

    public class LoadType
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }

    public class Classx
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }

    public class Equipment
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }

    public class Length
    {
        public string Text { get; set; }
        public string Value { get; set; }
    }   
}