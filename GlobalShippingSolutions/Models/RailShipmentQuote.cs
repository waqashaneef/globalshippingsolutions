﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GlobalShippingSolutions.Models
{
    public class RailShipmentQuote : GenericQuoteModel
    {
        [MaxLength(100)]
        [Display(Name = "No. of Pallets, Rolls or Boxes: ")]
        [Required(ErrorMessage = "* Required")]
        [StringLength(100, ErrorMessage = "* Length")]
        public string PalletsRollBoxes { set; get; }      
               

        [Display(Name = "Please Describe What You Are Shipping: ")]
        public string ShippingDescription { set; get; }
        
    }

    public static class RailShipmentQuoteExtentions
    {
        public static RailShipmentQuote PopulateDropDownLists(this RailShipmentQuote model)
        {

            model.LoadTypeList = new List<LoadType>
            {
                new LoadType() {Value = "Palletized", Text = "Palletized"},
                new LoadType() {Value = "Rolls", Text = "Rolls"},
                new LoadType() {Value = "Balles", Text = "Balles"},
                new LoadType() {Value = "Floor", Text = "Floor"}, 
                new LoadType() {Value = "Other", Text = "Other"} 
            };            

            return model;
        }

    }
}