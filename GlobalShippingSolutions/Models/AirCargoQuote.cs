﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GlobalShippingSolutions.Models
{
    public class AirCargoQuote: GenericQuoteModel
    {
        [MaxLength(100)]
        [Display(Name = "Dimensions: ")]
        [Required(ErrorMessage = "* Required")]
        [StringLength(100, ErrorMessage = "* Length")]        
        public string Dimensions { set; get; }

        public IEnumerable<Classx> ClassList { set; get; }

        [Required(ErrorMessage = "* Required")]
        [MaxLength(50)]
        [Display(Name = "Class: ")]
        public string Classx { set; get; }


        [Display(Name = "Comments: ")]
        public string Comments { set; get; }
    }
   
    public static class AirCargoQuoteExtentions
    {
        public static AirCargoQuote PopulateDropDownLists(this AirCargoQuote model)
        {

            model.LoadTypeList = new List<LoadType>
            {
                new LoadType() {Value = "Palletized", Text = "Palletized"},
                new LoadType() {Value = "Rolls", Text = "Rolls"},
                new LoadType() {Value = "Balles", Text = "Balles"},
                new LoadType() {Value = "Floor", Text = "Floor"}, 
                new LoadType() {Value = "Other", Text = "Other"} 
            };

            model.ClassList = new List<Classx>
            {
                new Classx() {Value = "Class 50", Text = "Class 50"},
                new Classx() {Value = "Class 55", Text = "Class 55"},
                new Classx() {Value = "Class 60", Text = "Class 60"},
                new Classx() {Value = "Class 65", Text = "Class 65"}, 
                new Classx() {Value = "Class 70", Text = "Class 70"}, 
                new Classx() {Value = "Class 77.5", Text = "Class 77.5"},
                new Classx() {Value = "Class 85", Text = "Class 85"},
                new Classx() {Value = "Class 92.5", Text = "Class 92.5"},
                new Classx() {Value = "Class 100", Text = "Class 100"},
                new Classx() {Value = "Class 110", Text = "Class 110"},
                new Classx() {Value = "Class 125", Text = "Class 125"},
                new Classx() {Value = "Class 150", Text = "Class 150"},
                new Classx() {Value = "Class 175", Text = "Class 175"},
                new Classx() {Value = "Class 200", Text = "Class 200"},
                new Classx() {Value = "Class 250", Text = "Class 250"},
                new Classx() {Value = "Class 300", Text = "Class 300"},
                new Classx() {Value = "Class 400", Text = "Class 400"},
                new Classx() {Value = "Class 500", Text = "Class 500"}
            };

            return model;
        }
    }
}