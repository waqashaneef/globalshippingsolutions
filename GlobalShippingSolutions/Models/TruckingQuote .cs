﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GlobalShippingSolutions.Models
{
    public class TruckingQuote : GenericQuoteModel
    {
        [MaxLength(100)]
        [Display(Name = "No. of Pallets, Rolls or Boxes: ")]
        [Required(ErrorMessage = "* Required")]
        [StringLength(100, ErrorMessage = "* Length")]
        public string PalletsRollBoxes { set; get; }

        public IEnumerable<Equipment> EquipmentList { set; get; }

        [Required(ErrorMessage = "* Required")]
        [MaxLength(50)]
        [Display(Name = "Equipment Required: ")]
        public string Equipment { set; get; }

        public IEnumerable<Length> LengthList { set; get; }

        [Required(ErrorMessage = "* Required")]
        [MaxLength(50)]
        [Display(Name = "Trailer Length: ")]
        public string Length { set; get; }


        [Display(Name = "Comments: ")]
        public string Comments { set; get; }

        [Display(Name = "Please Describe What You Are Shipping: ")]
        public string ShippingDescription { set; get; }
        
    }
   
    public static class TruckingQuoteExtentions
    {
        public static TruckingQuote PopulateDropDownLists(this TruckingQuote model)
        {

            model.LoadTypeList = new List<LoadType>
            {                
                new LoadType() {Value = "Rolls", Text = "Rolls"},
                new LoadType() {Value = "Balles", Text = "Balles"},
                new LoadType() {Value = "Floor", Text = "Floor"}, 
                new LoadType() {Value = "Other", Text = "Other"} 
            };

            model.EquipmentList = new List<Equipment>
            {
                new Equipment() {Value = "Flatbed", Text = "Flatbed"},
                new Equipment() {Value = "Dry Van", Text = "Dry Van"},
                new Equipment() {Value = "Reefer", Text = "Reefer"}, 
                new Equipment() {Value = "Heated Van", Text = "Heated Van"},
                new Equipment() {Value = "Flatbed w/Traps", Text = "Flatbed w/Traps"} 
            };

            model.LengthList = new List<Length>
            {
                new Length() {Value = "Van 48'", Text = "Van 48'"},
                new Length() {Value = "Van 53'", Text = "Van 53'"}                
            };

            return model;
        }

    }
}