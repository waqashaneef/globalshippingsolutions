﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace GlobalShippingSolutions.Models
{
    public class ResultModel
    {
        public bool Success { get; set; }
        public int ErrorCode { get; set; }
        public string ErrorMessage { get; set; }        
    }
    public class GenericResultModel: ResultModel
    {
        public object ModelErrorDetails { get; set; }
    }
 
}

//ErrorCode List
//10 = "Model Invalid"
