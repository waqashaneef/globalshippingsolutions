﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace GlobalShippingSolutions.Models
{
    public class FeedbackModel
    {        
        [MaxLength(100)]
        [Display(Name = "Contact Name: ")]
        [Required(ErrorMessage = "* Required")]
        [StringLength(100, ErrorMessage = "* Length")]  
        public string Name { set; get; }

        [MaxLength(250)]
        [Display(Name = "Email: ")]
        [Required(ErrorMessage = "* Required")]
        [StringLength(250, ErrorMessage = "* Length")]        
        [RegularExpression(@"^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$", ErrorMessage = "* Invalid Value")]
        public string Email { set; get; }

        [MaxLength(20)]
        [Display(Name = "Phone: ")]
        [Required(ErrorMessage = "* Required")]
        [StringLength(20, ErrorMessage = "* Length")]       
        [RegularExpression(@"^[0-9]+$", ErrorMessage = "* Invalid Value")]
        public string Phone { set; get; }

        [Required(ErrorMessage = "* Required")]
        [Display(Name = "Comments: ")]
        public string Comments { set; get; }
    }

}