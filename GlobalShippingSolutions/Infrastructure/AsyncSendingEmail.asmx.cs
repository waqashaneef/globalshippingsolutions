﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Threading;
using System.Net.Mail;
using System.Net;

using GlobalShippingSolutions.Models;
using System.Configuration;
using System.Net.Configuration;

namespace GlobalShippingSolutions.Infrastructure
{
    public class AsyncSendingEmail : System.Web.Services.WebService
    {

        public AsyncSendingEmail(EmailModel email, bool bodyHtml)
        {
            this.email = email;
            this.bodyHtml = bodyHtml;
        }

        private EmailModel email;
        private bool bodyHtml;
       
        [WebMethod]
        public void SendHtmlEmail()
        {            
            //GoDaddy
            //string fromAddress = "contactus@preferno.com";

            //var smtp = new System.Net.Mail.SmtpClient("relay-hosting.secureserver.net", 25);
            //{
            //    smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            //    smtp.Credentials = CredentialCache.DefaultNetworkCredentials;
            //}           

            //Gmail
            //string fromAddress = "waqas.haneef1@gmail.com";
            //string fromPassword = "quvlrjmygnodcprh";

            //var smtp = new System.Net.Mail.SmtpClient();
            //{
            //    smtp.Host = "smtp.gmail.com";
            //    smtp.Port = 587;
            //    smtp.EnableSsl = true;
            //    smtp.UseDefaultCredentials = false;
            //    smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            //    smtp.Credentials = new NetworkCredential(fromAddress, fromPassword);
            //}

            //SendGrid
            SmtpClient smtpClient = new SmtpClient();            
            var smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");         

            MailMessage mailMessage = new MailMessage(smtpSection.From, email.To, email.Subject, email.Body);
            mailMessage.IsBodyHtml = true;
            mailMessage.IsBodyHtml = true;
            smtpClient.SendAsync(mailMessage, null);


        }
        [WebMethod]
        public void SendAsynchronously()
        {
            ThreadStart ts = new ThreadStart(SendHtmlEmail);
            Thread thread = new Thread(ts);
            thread.Start();
            thread.Priority = ThreadPriority.Lowest;

        }
    }
}
