﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using System.Web.Mvc.Html;

namespace GlobalShippingSolutions.Infrastructure
{
    public static class CustomHelpers
    {
        public static MvcHtmlString MenuLink(this HtmlHelper htmlHelper, string linkText, string actionName, string controllerName )
        {
            var url = new UrlHelper(htmlHelper.ViewContext.RequestContext);
            string currentAction = htmlHelper.ViewContext.RouteData.GetRequiredString("action");
            string currentController = htmlHelper.ViewContext.RouteData.GetRequiredString("controller"); 

            // build the <a> tag
            var anchorBuilder = new TagBuilder("a");
            anchorBuilder.MergeAttribute("href", url.Action(actionName, controllerName));            
            anchorBuilder.InnerHtml = linkText;
            string anchorHtml = anchorBuilder.ToString(TagRenderMode.Normal);

            // build the <li> tag
            var listItemBuilder = new TagBuilder("li");
            listItemBuilder.InnerHtml += anchorHtml; // include the <a> tag inside

            if (actionName == currentAction && controllerName == currentController)            
            {
                listItemBuilder.MergeAttribute("class", "active");
            }

            return MvcHtmlString.Create(listItemBuilder.ToString(TagRenderMode.Normal));       
        }
    }

}